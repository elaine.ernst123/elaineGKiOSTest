//
//  Owner.swift
//  StackExchange
//
//  Created by Elaine on 2016/09/19.
//  Copyright © 2016 Elaine. All rights reserved.
//


import Foundation

public class Owner {
	public var reputation : Int?
	public var user_id : Int?
	public var user_type : String?
	public var accept_rate : Int?
	public var profile_image : String?
	public var display_name : String?
	public var link : String?

/**
    Returns an array of models based on given dictionary.

*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Owner]
    {
        var models:[Owner] = []
        for item in array
        {
            models.append(Owner(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.

*/
	required public init?(dictionary: NSDictionary) {

		reputation = dictionary["reputation"] as? Int
		user_id = dictionary["user_id"] as? Int
		user_type = dictionary["user_type"] as? String
		accept_rate = dictionary["accept_rate"] as? Int
		profile_image = dictionary["profile_image"] as? String
		display_name = dictionary["display_name"] as? String
		link = dictionary["link"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.reputation, forKey: "reputation")
		dictionary.setValue(self.user_id, forKey: "user_id")
		dictionary.setValue(self.user_type, forKey: "user_type")
		dictionary.setValue(self.accept_rate, forKey: "accept_rate")
		dictionary.setValue(self.profile_image, forKey: "profile_image")
		dictionary.setValue(self.display_name, forKey: "display_name")
		dictionary.setValue(self.link, forKey: "link")

		return dictionary
	}

}