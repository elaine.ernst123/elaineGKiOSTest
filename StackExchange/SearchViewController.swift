//
//  SearchViewController.swift
//  StackExchange
//
//  Created by Elaine on 2016/09/19.
//  Copyright © 2016 Elaine. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate
{
    // MARK: Properties
    lazy   var searchBar:UISearchBar = UISearchBar(frame: CGRectMake(0, 30, 200, 20))

//    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var searchResultsTableView: UITableView!
    
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    var searchDictionary : NSMutableDictionary = [:]
    var stackExchangeItem : StackExchangeManager!
    override func viewDidLoad() {
        super.viewDidLoad()
 
    
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.searchBar.delegate = self
        self.searchBar.tintColor = UIColor.whiteColor()
        self.searchBar.placeholder = "Search"
        self.navigationBar.topItem?.titleView = self.searchBar

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if stackExchangeItem != nil
        {
            
        return self.stackExchangeItem.items!.count
        }
        
        return 0;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("searchCell") as! SearchCell

        cell.contentView.backgroundColor = UIColor(netHex:0xEDEDED)

        
        let whiteRoundedView : UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width , 100))
        
        whiteRoundedView.layer.backgroundColor = UIColor(netHex:0xEDEDED).CGColor
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
        
        cell.whiteView.layer.borderColor = UIColor(netHex:0x727272).CGColor
        cell.whiteView.layer.borderWidth = 0.5
        let stackItem = self.stackExchangeItem.items! [indexPath.row]
        let questionOwner = stackItem.owner
        
        cell.question!.text = stackItem.title
        let color2 = UIColor(netHex:0x4078C4)

        cell.question.textColor = color2
        cell.answers.text = String (format: "%d answers", stackItem.answer_count!)
        cell.views.text = String (format: "%d views", stackItem.view_count!)
        cell.voted.text = String (format: "%d votes", stackItem.score!)
        
        cell.askedBy.text = String (format: "asked by %@", questionOwner!.display_name!)
        if ((stackItem.is_answered?.boolValue) != nil)
        {
            cell.checkMark.hidden = (stackItem.is_answered?.boolValue)!
            if cell.checkMark.hidden == true
            {
                cell.titleContraint.constant = 15
                cell.askedByContraint.constant = 20
            }
        }
        
        
        return cell
    }
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let stackItem = self.stackExchangeItem.items! [indexPath.row]
        
        let quesitonViewController = self.storyboard!.instantiateViewControllerWithIdentifier("QuestionViewController") as! QuestionViewController
        quesitonViewController.stackItem = stackItem
        self.navigationController!.pushViewController(quesitonViewController, animated: true)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        if CommonHelper.isConnectedToNetwork()
        {
            self.activityIndicator .startAnimating()
            let searchString = searchBar.text
            let urlString = String (format:"/2.2/search?order=desc&sort=activity&tagged=%@&site=stackoverflow&filter=withbody",searchString!)
            NetworkManager.sharedInstance .sendRequest(self.searchDictionary, requestType: "GET", requestUrl: urlString,
                                                       completion: { (dictionary, error) -> () in
            // do stuff with the result
           self.stackExchangeItem = StackExchangeManager(dictionary: dictionary)
            
            
            dispatch_async(dispatch_get_main_queue()){
                self.activityIndicator.stopAnimating()
                /* Do UI work here */
                if self.stackExchangeItem.items?.count  > 0
                {
                
                self.searchResultsTableView .reloadData()
                self.searchResultsTableView.hidden = false
                }
                else
                {
                    self.searchLabel.text = "Could not find any results."
                }

                
            }

            })
        }
        else
        {
            let alertController = UIAlertController(title: "Error", message: "Internet connection apprears to be offline.", preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in

            }
            alertController.addAction(cancelAction)
            
   
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }        }
        

    }
    
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        dispatch_async(dispatch_get_main_queue()){
            /* Do UI work here */
            self.searchResultsTableView.hidden = true
            self.searchLabel.text = "Stack Exchange Search"
    }
    }
    
}