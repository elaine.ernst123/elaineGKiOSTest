
//
//  Items.swift
//  StackExchange
//
//  Created by Elaine on 2016/09/19.
//  Copyright © 2016 Elaine. All rights reserved.
//

import Foundation
 

public class Items {
    public var tagsArray = [String]()
	public var owner : Owner?
	public var is_answered : Bool?
	public var view_count : Int?
	public var accepted_answer_id : Int?
	public var answer_count : Int?
	public var score : Int?
	public var last_activity_date : Int?
	public var creation_date : Int?
	public var last_edit_date : Int?
	public var question_id : Int?
	public var link : String?
	public var title : String?
	public var body : String?

/**
    Returns an array of models based on given dictionary.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Items]
    {
        var models:[Items] = []
        for item in array
        {
            models.append(Items(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
*/
	required public init?(dictionary: NSDictionary) {

        if let dataArray = dictionary["tags"] as? NSArray {
            let stringArray = dataArray.map
                {
                    (String($0))
            }
            tagsArray = stringArray
        }
        
		if (dictionary["owner"] != nil) { owner = Owner(dictionary: dictionary["owner"] as! NSDictionary) }
		is_answered = dictionary["is_answered"] as? Bool
		view_count = dictionary["view_count"] as? Int
		accepted_answer_id = dictionary["accepted_answer_id"] as? Int
		answer_count = dictionary["answer_count"] as? Int
		score = dictionary["score"] as? Int
		last_activity_date = dictionary["last_activity_date"] as? Int
		creation_date = dictionary["creation_date"] as? Int
		last_edit_date = dictionary["last_edit_date"] as? Int
		question_id = dictionary["question_id"] as? Int
		link = dictionary["link"] as? String
		title = dictionary["title"] as? String
		body = dictionary["body"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.owner?.dictionaryRepresentation(), forKey: "owner")
        dictionary.setValue(self.tagsArray, forKey: "tags")

		dictionary.setValue(self.is_answered, forKey: "is_answered")
		dictionary.setValue(self.view_count, forKey: "view_count")
		dictionary.setValue(self.accepted_answer_id, forKey: "accepted_answer_id")
		dictionary.setValue(self.answer_count, forKey: "answer_count")
		dictionary.setValue(self.score, forKey: "score")
		dictionary.setValue(self.last_activity_date, forKey: "last_activity_date")
		dictionary.setValue(self.creation_date, forKey: "creation_date")
		dictionary.setValue(self.last_edit_date, forKey: "last_edit_date")
		dictionary.setValue(self.question_id, forKey: "question_id")
		dictionary.setValue(self.link, forKey: "link")
		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.body, forKey: "body")

		return dictionary
	}

}