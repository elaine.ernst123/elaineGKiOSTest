//
//  SearchCell.swift
//  StackExchange
//
//  Created by Elaine on 2016/09/23.
//  Copyright © 2016 Elaine. All rights reserved.
//

import Foundation
import UIKit
class SearchCell: UITableViewCell {

    @IBOutlet weak var checkMark: UIImageView! = nil
    @IBOutlet weak var question: UITextView!
    @IBOutlet weak var askedBy: UILabel!
    @IBOutlet weak var answers: UILabel!
    @IBOutlet weak var views: UILabel!
    @IBOutlet weak var voted: UILabel!
    
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var titleContraint: NSLayoutConstraint!
    
    @IBOutlet weak var askedByContraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}