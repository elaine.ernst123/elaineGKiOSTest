//
//  NetworkManager.swift
//  StackExchange
//
//  Created by Elaine on 2016/09/19.
//  Copyright © 2016 Elaine. All rights reserved.
//

import Foundation
import UIKit

class NetworkManager: NSObject, NSURLSessionDelegate {
    
    static let sharedInstance = NetworkManager()
     let URL : String = "https://api.stackexchange.com"
        var backgroundSession: NSURLSession!
    func sendRequest(requestBody: NSDictionary ,requestType : String, requestUrl: String, completion: ((success:NSMutableDictionary , error:NSError?) ->())) -> Void
        {
        
        let url = String (format: "%@%@",self.URL, requestUrl)
        let myURL = NSURL(string: url)
        let session = NSURLSession.sharedSession()
        
        let request = NSMutableURLRequest(URL: myURL!)
        request.HTTPMethod = requestType
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        if (requestBody.count > 0)
        {
        let dataExample : NSData = NSKeyedArchiver.archivedDataWithRootObject(requestBody)
        request.HTTPBody = dataExample
        }
        let task = session.dataTaskWithRequest(request) {
            (
            let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("error")
                return
            }
            
            // convert NSData to 'AnyObject'
            do {
                if let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSMutableDictionary
                    
                {
                    
                    completion(success:jsonResult, error: error)

                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        task.resume()
        
        }
    
    func downloadImageFromUrl(urlImage:NSString, onfinished:(UIImage) -> ()) {
        
        let backgroundSessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier("backgroundSession")
    
        let url = NSURLRequest(URL:  NSURL(string:urlImage as String)!);
        backgroundSession = NSURLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: NSOperationQueue.mainQueue())

        let downloadTask = backgroundSession.dataTaskWithRequest(url, completionHandler: {
            data,response,error in
            
            if error == nil
            {
                if let image = UIImage(data: data!)
                {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        onfinished(image)
                    })
                }
            }
            
        })
        downloadTask.resume();
        
    }
}