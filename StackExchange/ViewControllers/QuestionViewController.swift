//
//  QuestionViewController.swift
//  StackExchange
//
//  Created by Elaine on 2016/09/25.
//  Copyright © 2016 Elaine. All rights reserved.
//

import Foundation
import UIKit
class QuestionViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var tagsLabel: UILabel!
    
    @IBOutlet weak var displayName: UILabel!
    
    @IBOutlet weak var dateCreated: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var questionTitleTextView: UITextView!
    
    @IBOutlet weak var profileImage: UIImageView!
  
    @IBOutlet weak var questionBodyTextView: UITextView!
    var stackExchangeItem : StackExchangeManager!
    var stackItem : Items?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated) 
    // Set textfield UI
        
        dispatch_async(dispatch_get_main_queue())
        {
        self.questionTitleTextView.textColor = UIColor(netHex:0x727272)
        self.questionTitleTextView.layer.borderColor = UIColor.blackColor().CGColor
        self.questionTitleTextView.layer.borderWidth = 0.5
        self.questionTitleTextView.textContainerInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        self.questionTitleTextView.selectable = false
        self.questionBodyTextView.textColor = UIColor(netHex:0x727272)
        self.questionBodyTextView.scrollsToTop = true
        self.questionBodyTextView.textContainerInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        self.questionBodyTextView.selectable = false
        }
        let owner = self.stackItem?.owner
        let newArray = self.stackItem?.tagsArray

        let joined = newArray?.joinWithSeparator(",")

        // Formate date
        let dateString : Int = (self.stackItem?.creation_date!)!
        let ti = NSTimeInterval(dateString)

        let date = NSDate(timeIntervalSince1970: ti)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .FullStyle
        dateFormatter.timeStyle = .ShortStyle
        let dateStringNew = dateFormatter.stringFromDate(date)
        
        // get string value from HTML body
        self.questionBodyTextView.text = stringFromHTML(self.stackItem?.body)

        
        self.questionTitleTextView.text = self.stackItem?.title
        self.dateCreated.text = dateStringNew
        self.tagsLabel.text = joined
        self.displayName.text = owner?.display_name
        self.score.text = String(format: "%d", (self.stackItem?.score)!)
        
        // Load profile image
        self.loadImageFromUrl((owner?.profile_image)!, view: self.profileImage)
    }
    func stringFromHTML( string: String?) -> String
    {
        do{
            let str = try NSAttributedString(data:string!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true
                )!, options:[NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(unsignedLong: NSUTF8StringEncoding)], documentAttributes: nil)
            return str.string
        } catch
        {
            print("html error\n",error)
        }
        return ""
    }
    
    
   func loadImageFromUrl(url: String, view: UIImageView){
        
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    view.image = UIImage(data: data)
                })
            }
        }
        
        // Run task
        task.resume()
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        // custom back button handler
        self.navigationController?.popViewControllerAnimated(true)
    }
}