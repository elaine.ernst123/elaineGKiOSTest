//
//  StackExchangeManager.swift
//  StackExchange
//
//  Created by Elaine on 2016/09/19.
//  Copyright © 2016 Elaine. All rights reserved.
//

import Foundation
 

public class StackExchangeManager{
	public var items : Array<Items>?
	public var has_more : Bool?
	public var quota_max : Int?
	public var quota_remaining : Int?

/**
    Returns an array of models based on given dictionary.

*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [StackExchangeManager]
    {
        var models:[StackExchangeManager] = []
        for item in array
        {
            models.append(StackExchangeManager(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
*/
	required public init?(dictionary: NSDictionary) {

		if (dictionary["items"] != nil) { items = Items.modelsFromDictionaryArray(dictionary["items"] as! NSArray) }
		has_more = dictionary["has_more"] as? Bool
		quota_max = dictionary["quota_max"] as? Int
		quota_remaining = dictionary["quota_remaining"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.has_more, forKey: "has_more")
		dictionary.setValue(self.quota_max, forKey: "quota_max")
		dictionary.setValue(self.quota_remaining, forKey: "quota_remaining")

		return dictionary
	}

}